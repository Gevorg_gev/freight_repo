using System.Runtime.InteropServices;
using System;

namespace WebApplication6
{
    public class Freight
    {
        public int Id { get; set; }
        public int Massive { get; set; }
        public string CompanyName { get; set; }
        public string Country { get; set; }
        public string City { get; set; } 
    }
}
