using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication6
{
    [ApiController]
    [Route("[controller]")]

    public class FreightController : ControllerBase
    {

        private static readonly string[] Summaries1 = new[]
        {
            "ABF U-Pack Moving" , "Abiline Motor Express" , "Able Freight Service" , "Active Transportation"
        };

        private static readonly string[] Summaries2 = new[]
        {
            "United States of America" , "Unitet Arab Emirates" , "Russia" , "Australia" , "Germany"
        };

        private static readonly string[] Summaries3 = new[]
        {
            "Washington" , "Berlin" , "Tokyo" , "Moscow" , "Sydnney" , "Hambourg" , "Singapore"
        };

        [HttpGet]
        public IEnumerable<Freight> GetInfo()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new Freight
            {
                Id = rng.Next(1, 20),
                Massive = rng.Next(1, 250),
                CompanyName = Summaries1[rng.Next(Summaries1.Length)],
                Country = Summaries2[rng.Next(Summaries2.Length)],
                City = Summaries3[rng.Next(Summaries3.Length)]
            });

        }

        [HttpPost]

        public IActionResult AddFreight([FromBody] Freight freight)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            using (var dbContext = new FreightDbContext())
            {
                dbContext.Freights.Add(freight);
                dbContext.SaveChanges();
            }
            return Ok();
        }

        [HttpDelete("{Id}")]

        public IActionResult RemoveFreight([FromRoute] int id)
        {
            using (var dbContext = new FreightDbContext())
            {
                var freight = dbContext.Freights.Find(id);

                if (freight == null)
                {
                    return BadRequest("Wrong Freight Id!");
                }

                dbContext.Freights.Remove(freight);
                dbContext.SaveChanges();
            }
            return Ok();
        }

        [HttpPut("{id}")]  
        public IActionResult UpdateFreight([FromRoute] int id, [FromBody] Freight freight)
        {
            using (var dbContext = new FreightDbContext())
            {
                var freightToUpdate = dbContext.Freights.Find(id);

                if(freightToUpdate == null)
                {
                    return BadRequest("Wrong Freight Id!");
                }

                freightToUpdate.Id = freight.Id;
                freightToUpdate.Massive = freight.Massive;
                freightToUpdate.CompanyName = freight.CompanyName;
                freightToUpdate.Country = freight.Country;
                freightToUpdate.City = freight.City; 

                dbContext.Freights.Update(freightToUpdate); 
                dbContext.SaveChanges(); 
            }

                return Ok();
        } 
    }
}
