using System.Runtime.InteropServices;
using System;
using Microsoft.EntityFrameworkCore;

namespace WebApplication6
{
    public class FreightDbContext : DbContext
    {
        public DbSet<Freight> Freights { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=LAPTOP-R91MV32G;Database=FreightDB;Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }  

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Freight>(x =>
            {
                x.ToTable("Freights");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd(); 
            });
            base.OnModelCreating(modelBuilder);
        }
    }
}
